<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    public function showForgetPasswordForm()
    {
        return view('forgotpassword');
    }

    public function submitForgetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
        ]);

        $token = Str::random(64);

        DB::table('password_reset_tokens')->where('email', $request->email)->delete();

        DB::table('password_reset_tokens')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        Mail::send(
            'mail-reset-password',
            [
                'token' => $token
            ],
            function ($message) use ($request) {
                $message->to($request->email);
                $message->subject('Reset Password');
            }
        );

        return back()->with('message', 'Kami sudah mengirim link reset passwordmu!');
    }

    public function showResetPasswordForm($token)
    {
        $passwordReset = DB::table('password_reset_tokens')->where('token', $token)->first();

        if (!$passwordReset) {
            return redirect()->route('login')->with('error', 'Invalid token');
        }

        return view('changepassword', ['token' => $token, 'email' => $passwordReset->email]);
    }

    public function submitResetPasswordForm(Request $request)
    {
        $request->validate([
            'password'  =>  'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        $updatePassword = DB::table('password_reset_tokens')
            ->where([
                'email' => $request->email,
                'token' => $request->token
            ])
            ->first();

        if (!$updatePassword) {
            return back()->withInput()->with('error', 'Invalid Token');
        }

        $user = User::where('email', $request->email)
            ->first();

        if (!$user) {
            return back()->withInput()->with('error', 'Email tidak terdaftar');
        }

        $user->password = Hash::make($request->password);
        $user->save();

        DB::table('password_reset_tokens')->where(['email' => $request->email])->delete();

        return redirect()->route('login')->with('message', 'Passwordmu Berhasil Dirubah!');
    }
}
