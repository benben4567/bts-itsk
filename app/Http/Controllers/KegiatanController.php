<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kegiatan;
use App\Models\Provinsi;

class KegiatanController extends Controller
{
    public function indexForm()
    {
        $kegiatan = Kegiatan::get();
        //$kegiatan = Kegiatan::paginate(5);
        return view("admin.kegiatan.kegiatan_index", compact("kegiatan"));
    }
     // public function search(Request $request)
    // {
    //     if ($request->ajax()) {
    //         $search = $request->get('search');
    //         $kegiatan = Kegiatan::whereHas('users', function ($query) use ($search) {
    //             $query->where('nama', 'like', '%' . $search . '%');
    //         })->orWhere('sekolah', 'like', '%' . $search . '%')
    //             ->orWhere('tanggal_kegiatan', 'like', '%' . $search . '%')
    //             ->orWhereHas('provinsi', function ($query) use ($search) {
    //                 $query->where('provinsi', 'like', '%' . $search . '%');
    //             })->get();

    //         $kegiatan->load('users', 'provinsi');
    //         return response()->json($kegiatan);
    //     }
    // }
    public function liveSearch(Request $request)
    {
        $search = $request->input('search');

        $kegiatan = Kegiatan::with(['users', 'provinsi'])
            ->when($search, function ($query, $search) {
                $query->whereHas('users', function ($query) use ($search) {
                    $query->where('nama', 'like', '%' . $search . '%');
                })
                    ->orWhere('sekolah', 'like', '%' . $search . '%')
                    ->orWhere('tanggal_kegiatan', 'like', '%' . $search . '%')
                    ->orWhereHas('provinsi', function ($query) use ($search) {
                        $query->where('provinsi', 'like', '%' . $search . '%');
                    });
            })
            ->paginate(2);

        return view('admin.kegiatan.kegiatan_index', compact('kegiatan', 'search'));
    }

    public function createForm()
    {
        $provinsi = Provinsi::all();
        return view("admin.kegiatan.kegiatan_create", compact("provinsi"));
    }

    public function storeKegiatan(Request $request) // CREATE UNTUK UJI COBA CREATE, AGAR TIDAK PERLU PINDAH-PINDAH USER
    {
        $request->validate([
            'id_provinsi' => 'required',
            'tanggal_kegiatan' => 'required|date',
            'sekolah' => 'required',
            'status_promosi' => 'required|in:Diterima,Diproses,Ditolak',
            'catatan_promosi' => 'nullable',
        ]);

        Kegiatan::create($request->all());

        return redirect()->route('admin.kegiatan.index')->with('success', 'Kegiatan berhasil ditambahkan.');
    }

    public function editForm(Kegiatan $kegiatan)
    {
        $provinsi = Provinsi::all();
        return view('admin.kegiatan.kegiatan_edit', compact('kegiatan', 'provinsi'));
    }

    public function updateKegiatan(Request $request, Kegiatan $kegiatan)
    {
        $request->validate([
            'status_promosi' => 'required|in:Diterima,Diproses,Ditolak',
            'catatan_promosi' => 'nullable',
        ]);

        $kegiatan->update($request->all());
        return redirect()->route('admin.kegiatan.index')->with('success', 'Kegiatan berhasil diperbarui.');
    }

    public function destroyKegiatan(Kegiatan $kegiatan)
    {

        $kegiatan->users()->detach();

        // Hapus data kegiatan
        $kegiatan->delete();

        return redirect()->route('admin.kegiatan.index')->with('success', 'Kegiatan berhasil dihapus.');
    }

    public function show($id)
    {
        // Memuat kegiatan beserta user yang terlibat
        $kegiatan = Kegiatan::with('users')->findOrFail($id);

        // Mendapatkan nama Ketua
        $ketua = $kegiatan->users()->wherePivot('jabatan', 'Ketua')->first();
        $ketuaName = $ketua ? $ketua->nama : 'Tidak ada';

        // Mendapatkan nama Anggota
        $anggota = $kegiatan->users()->wherePivot('jabatan', 'Anggota')->get();
        $anggotaNames = $anggota->pluck('nama');

        // Mendapatkan nama Dosen Pembimbing
        $dosen = $kegiatan->users()->wherePivot('jabatan', 'Dosen')->first();
        $dosenName = $dosen ? $dosen->nama : 'Tidak ada';

        // Mendapatkan nama sekolah dan tanggal kegiatan
        $sekolah = $kegiatan->sekolah;
        $tanggalKegiatan = $kegiatan->tanggal_kegiatan;

        return view('admin.kegiatan.show', compact('ketuaName', 'anggotaNames', 'dosenName', 'sekolah', 'tanggalKegiatan'));
    }

    public function index(Request $request)
    {
        $search = $request->input('search');
    
        // Query data kegiatan dengan atau tanpa pencarian
        $query = Kegiatan::query();
        if (!empty($search)) {
            $query->where('sekolah', 'like', '%' . $search . '%');
        }
        $kegiatans = $query->get();
    
        return view('admin.kegiatan.kegiatanfilter', compact('kegiatans', 'search'));
    }

}
