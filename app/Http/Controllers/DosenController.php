<?php

namespace App\Http\Controllers;

use App\Models\kegiatan;
use App\Models\laporan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DosenController extends Controller
{
    //view
    public function view_kegiatan()
    {
        $title = "Kegiatan";
        $kgt['kegiatan'] = kegiatan::all();
        return view('dosen.kegiatan.kegiatan_index', $kgt, compact('title'));
    }

    public function kegiatan_edit($id)
    {
        $editData = kegiatan::find($id);
        return view('dosen.kegiatan.kegiatan_edit', compact('editData'));
    }
    //edit_kegiatan
    public function updatekegiatan(Request $request, $id)
    {
        $request->validate([
            'status_promosi' => 'required|in:Diterima,Diproses,Ditolak',
        ]);

        $kegiatan = kegiatan::findOrFail($id);
        $kegiatan->status_promosi = $request->status_promosi;
        $kegiatan->catatan_promosi = $request->catatan_promosi;
        $kegiatan->save();

        return redirect()->route('dosen.view')->with('success', 'Status promosi kegiatan berhasil diperbarui');
    }
    //delete_kegiatan
    public function deletekegiatan(Kegiatan $kegiatan)
    {
        $kegiatan->users()->detach();

        $kegiatan->delete();
        return redirect()->route('dosen.view')->with('success', 'Status promosi kegiatan berhasil dihapus');
    }

    // LAPORAN
    public function indexLaporan()
    {
        // $title = "Laporan";
        // $laporan = laporan::all();
        $laporans = Laporan::with(['kegiatan.users' => function ($query) {
            $query->where('jabatan', 'Ketua');
        }])->paginate(5);
        return view("dosen.laporan.laporan_index", compact("laporans"));
    }

    public function editLaporan($id)
    {
        $laporan = laporan::findOrFail($id);
        return view("dosen.laporan.laporan_edit", compact("laporan"));
    }

    public function updateLaporan(Request $request, $id)
    {
        $request->validate([
            "status_promosi" => "required|in:Diterima,Diproses,Ditolak",
            // "tanggal_laporan" => "required|date|date_format:Y-m-d\TH:i:s",
            "catatan" => "nullable",
        ]);

        $laporan = Laporan::findOrFail($id);
        $laporan->update($request->all());

        return redirect()->route("dosen.laporan.index")->with("success", "Laporan berhasil diperbarui.");
    }

    public function downloadLaporan($id)
    {
        $laporan = laporan::findOrFail($id);
        if (!$laporan) {
            abort(404);
        }

        $filePaths = [];
        foreach ($laporan->files as $file) {
            $filePaths[] = Storage::path($file->dokumen);
        }

        foreach ($filePaths as $filePath) {
            readfile($filePath);
        }
    }

    public function dashboard()
    {
        $title = "Dashboard Dosen";
        return view("dosen.dosen_dashboard", compact("title"));
    }
}
