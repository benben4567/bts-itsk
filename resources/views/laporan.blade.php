<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap 5 CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">

    <title>BTS-ITSK | Laporan</title>
</head>

<body>
    <div class="container">
        <h1 class="col-12 col-sm-8 col-md-6 col-lg-5 h2 text-center mx-auto mt-5 pb-2 border-bottom border-2">Laporan</h1>

        <form action="#" method="post" class="col-11 col-sm-7 col-md-5 col-lg-4 mx-auto my-5">
            <label for="absen" class="h5">Absen</label> <br>
            <div class="mb-4">
                <label class="btn bg-body-secondary fw-semibold shadow-sm" for="absen">
                    <i class="bi bi-upload"></i>
                    <span style="margin-left: 10px;">Upload Absen</span>
                </label>
                <input type="file" class="d-none" id="absen" name="absen" required>
            </div>

            <label for="dokumentasi_kegiatan" class="h5">Dokumentasi Kegiatan</label> <br>
            <div class="mb-4">
                <label class="btn bg-body-secondary fw-semibold shadow-sm" for="dokumentasi_kegiatan">
                    <i class="bi bi-upload"></i>
                    <span style="margin-left: 10px;">Upload Dokumentasi</span>
                </label>
                <input type="file" class="d-none" id="dokumentasi_kegiatan" name="dokumentasi_kegiatan" required>
            </div>

            <button type="submit" class="btn btn-dark w-75 d-block mx-auto mt-5 py-3">Kirim</button>
        </form>
    </div>

    <!-- Bootstrap 5 JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>