@extends('login.loginform')

@section('title', 'BTS-ITSK | Sign Up')

@section('container')

    <div class="col-12 col-sm-10 col-xl-8 p-4 p-sm-5">
        <h1 class="fs-2 mb-5">Mulai Sekarang</h1>
        @include('sweetalert::alert')

        @if ($errors->any())
            <script>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops.., Sepertinya Data yang kamu masukkan tidak sesuai',
                    html: {!! json_encode(implode('<br>', $errors->all())) !!}
                });
            </script>
        @endif
        <form action="{{ route('register.submit') }}" method="post">
            @csrf
            <label for="nama" class="form-label fw-semibold mb-0">Nama</label>
            <input type="text" class="form-control mb-3 p-2" id="nama" placeholder="Masukkan nama" name="nama"
                required>

            <label for="nim" class="form-label fw-semibold mb-0">NIM</label>
            <input type="text" class="form-control mb-3 p-2" id="nim" placeholder="Masukkan nim" name="nim"
                required>

            <label for="email" class="form-label fw-semibold mb-0">Email</label>
            <input type="email" class="form-control mb-3 p-2" id="email" placeholder="Masukkan email" name="email"
                required>

            <label for="password" class="form-label fw-semibold mb-0">Password</label>
            <input type="password" class="form-control mb-3 p-2" id="password" placeholder="Masukkan password"
                name="password" required>

            <label for="prodi" class="form-label fw-semibold mb-0">Program Studi</label>
            <select class="form-select mb-3 p-2" id="prodi" name="prodi" required>
                <option value="" selected disabled>Pilih Program Studi</option>
                <option value="TRPL">Teknik Rekayasa Perangkat Lunak (TRPL)</option>
                <option value="TRK">Teknik Rekayasa Komputer (TRK)</option>
                <option value="BD">Bisnis Digital (BD)</option>
            </select>

            <input class="form-check-input" type="checkbox" id="ingat" name="ingat">
            <label for="ingat" class="form-check-label fw-semibold mb-3" style="font-size: 13px; margin-left: 5px;">Ingat
                selama 30
                hari</label>

            <button type="submit" class="btn btn-dark w-100 p-2">Daftar</button>
        </form>
        <p class="text-center fw-semibold mt-3" style="font-size: 13px;">Atau</p>
        <p class="text-center fw-semibold mt-5 mb-0" style="font-size: 13px;">Sudah mempunyai akun? <a
                href="{{ url('/signin') }}" class="text-primary text-decoration-none">Masuk</a></p>
    </div>



@endsection
