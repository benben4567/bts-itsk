@extends('dosen.layouts.main')

@php
    $title = 'Laporan';
@endphp

@section('title')
    Dashboard Laporan
@endsection

@section('sidebar_item')
    @include('dosen.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 col-sm-10 col-md-9 mx-auto mb-5 p-4 p-sm-5 border"
        style="background-color: rgb(255, 255, 255); margin-top: 125px; border-radius: 10px">
        <form action="{{ route('dosen.laporan.update', $laporan->id) }}" method="POST">
            @csrf
            @method('PUT')
            <h1 class="fs-5 mb-5 pb-2 border-bottom border-2">Edit Laporan</h1>
            <div class="mb-4">
                <label for="status_promosi" class="form-label fw-semibold">Status Promosi</label>
                <select name="status_promosi" id="status_promosi" class="form-select p-2">
                    <option value="Diterima">Diterima</option>
                    <option value="Diproses">Diproses</option>
                    <option value="Ditolak">Ditolak</option>
                </select>
            </div>

            <div class="mb-4">
                <label for="catatan" class="form-label fw-semibold">Catatan</label>
                <textarea name="catatan" id="catatan" rows="4" class="form-control">{{ $laporan->catatan_promosi }}</textarea>
            </div>

            <button type="submit" class="btn btn-dark d-block mx-auto mt-5 px-5 py-2">Simpan</button>
        </form>
    </div>
@endsection


{{-- <form action="{{ route('dosen.laporan.update', $laporan->id) }}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="status_promosi">Status Promosi</label>
        <select name="status_promosi" id="status_promosi" class="form-control">
            <option value="Diterima" {{ $laporan->status_promosi == 'Diterima' ? 'selected' : '' }}>Diterima</option>
            <option value="Diproses" {{ $laporan->status_promosi == 'Diproses' ? 'selected' : '' }}>Diproses</option>
            <option value="Ditolak" {{ $laporan->status_promosi == 'Ditolak' ? 'selected' : '' }}>Ditolak</option>
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
</form> --}}
