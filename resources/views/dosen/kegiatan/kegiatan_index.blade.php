@extends('dosen.layouts.main')

@section('title')
    {{ $title }}
@endsection

@section('sidebar_item')
    @include('dosen.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 mx-auto mb-5 border overflow-hidden"
        style="background-color: rgb(255, 255, 255); font-size: 13px; margin-top: 125px; border-radius: 10px">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center p-4">
            <h1 class="fs-5 mb-3 mb-sm-0">Data Kegiatan</h1>
            {{-- <a href="{{ route('admin.kegiatan.create') }}" class="btn btn-success align-self-end" style="border-radius: 20px; background-color: #4CAF50; color: white; padding: 3px 6px;">
                <span>+</span>
                <span>Tambah Data</span>
            </a> --}}
        </div>
        {{-- <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center text-secondary px-4">
            <form action="#" method="post" class="mb-3 mb-sm-0">
                <label for="show">Show</label>
                <input type="number" name="show" id="show" value="10" class="border border-2"
                    style="width: 75px; border-radius: 5px;">
                <span>entries</span>
            </form>
            <form action="#" method="post" class="">
                <label for="search">Search :</label>
                <input type="text" name="search" id="search" class="border border-2 px-1"
                    style="width: 200px; border-radius: 5px;">
            </form>
        </div> --}}

        {{-- Table --}}

        <div class="table-responsive px-3 pb-3">
            <table class="table table-hover mt-3" id="table-kegiatan">
                <thead class="table-light border-top border-bottom">
                    <tr>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">ID Kegiatan</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">Nama Ketua</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">Dosen</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">Provinsi</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">Tanggal Kegiatan</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">Sekolah</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">Status</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">Aksi</th>
                        {{-- <th class="text-secondary fw-semibold text-center px-3 text-nowrap">Tanggapan</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @if ($kegiatan->isEmpty())
                        <p>Tidak ada kegiatan.</p>
                    @else
                        @foreach ($kegiatan as $kegiatan)
                            <tr>
                                <td class="text-secondary text-center px-3 text-nowrap">{{ $loop->iteration }}</td>
                                <td class="px-3 text-nowrap">
                                    @foreach ($kegiatan->users()->where('jabatan', 'Ketua')->get() as $anggota)
                                        {{ $anggota->nama }}
                                    @endforeach
                                </td>
                                <td class="px-3 text-nowrap">
                                    @foreach ($kegiatan->users()->where('jabatan', 'Dosen')->get() as $anggota)
                                        {{ $anggota->nama }}
                                    @endforeach
                                </td>
                                <td class="px-3 text-nowrap">{{ $kegiatan->provinsi->provinsi }}</td>
                                <td class="px-3 text-nowrap">{{ $kegiatan->tanggal_kegiatan }}</td>
                                <td class="px-3 text-nowrap">{{ $kegiatan->sekolah }}</td>
                                <td class="px-3 text-center text-nowrap">
                                    @if ($kegiatan->status_promosi == 'Proses')
                                        <span class="badge text-bg-secondary fw-normal pb-2"
                                            style="font-size: 13px">Proses</span>
                                    @elseif($kegiatan->status_promosi == 'Diterima')
                                        <span class="badge text-bg-success fw-normal pb-2"
                                            style="font-size: 13px">Selesai</span>
                                    @elseif($kegiatan->status_promosi == 'Ditolak')
                                        <span class="badge text-bg-danger fw-normal pb-2"
                                            style="font-size: 13px">Ditolak</span>
                                    @else
                                        <span class="badge text-bg-secondary fw-normal pb-2"
                                            style="font-size: 13px">{{ $kegiatan->status_promosi }}</span>
                                    @endif
                                </td>
                                <td class="text-center px-3 text-nowrap">
                                    {{-- <form action="#" method="get" class="d-inline">
                                        <button type="submit" name="lihat" class="btn text-secondary fs-5 mx-1">
                                            <i class="bi bi-eye"></i>
                                        </button>
                                    </form> --}}
                                    <form action="{{ route('dosen.Edit', $kegiatan->id) }}" method="get" class="d-inline">
                                        <button type="submit" name="edit" class="btn  text-secondary fs-5 mx-1">
                                            <i class="bi bi-pencil-square"></i>
                                        </button>
                                    </form>
                                    <form action="{{ route('kegiatan.delete', $kegiatan->id) }}" method="post"
                                        class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" name="hapus" class="btn text-secondary fs-5 mx-1"
                                            onclick="return confirm('Apakah Anda yakin ingin menghapus kegiatan ini?')">
                                            <i class="bi bi-trash3"></i>
                                        </button>
                                    </form>
                                </td>
                                {{-- <td class="text-center px-3 text-nowrap">
                                    <form action="#" method="post" class="d-inline">
                                        @csrf
                                        @method('PATCH')
                                        <button type="submit" name="diterima" class="btn btn-success mx-1"
                                            style="font-size: 13px">
                                            <span>Diterima</span>
                                        </button>
                                        <button type="submit" name="ditolak" class="btn btn-danger mx-1"
                                            style="font-size: 13px">
                                            <span>Ditolak</span>
                                        </button>
                                    </form>
                                </td> --}}
                            </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
        @endif
        {{-- <div class="d-flex flex-column flex-md-row justify-content-between align-items-center text-secondary p-4">
            <div>Showing <span>1</span> to <span>10</span> of <span>50</span> entries</div>
            <nav class="mt-5 mt-md-0">
                <ul class="pagination mb-0">
                    <li class="page-item disabled">
                        <a class="page-link text-secondary">
                            <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                        </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link text-secondary" href="#">2</a></li>
                    <li class="page-item"><a class="page-link text-secondary" href="#">3</a></li>
                    <li class="page-item"><a class="page-link text-secondary" href="#">4</a></li>
                    <li class="page-item"><a class="page-link text-secondary" href="#">5</a></li>
                    <li class="page-item">
                        <a class="page-link text-secondary" href="#">
                            Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div> --}}
    </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table-kegiatan').DataTable();
        });
    </script>
@endpush