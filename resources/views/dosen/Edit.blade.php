<form action="{{ route('kegiatan.Edit', ['id' => $editData->id]) }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="status_promosi">Status Promosi:</label>
        <select name="status_promosi" id="status_promosi" class="form-control">
            <option value="Diterima" {{ $editData->status_promosi == 'Diterima' ? 'selected' : '' }}>Diterima</option>
            <option value="Diproses" {{ $editData->status_promosi == 'Diproses' ? 'selected' : '' }}>Diproses</option>
            <option value="Ditolak" {{ $editData->status_promosi == 'Ditolak' ? 'selected' : '' }}>Ditolak</option>
        </select>
        @error('status_promosi')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Perbarui</button>
</form>
