@extends('dosen.layouts.main')

@section('title')
    {{ $title }}
@endsection

@section('sidebar_item')
    @include('dosen.partials.sidebar')
@endsection