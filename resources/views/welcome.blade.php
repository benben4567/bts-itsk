@extends('layouts.main')

@section('title', 'BTS-ITSK | Selamat Datang')

@section('content')
    {{-- NAVBAR  --}}
    @include('partials.navbar')

    {{-- HEADER  --}}
    @include('partials.header')

    {{-- MANFAAT  --}}
    @include('partials.manfaat')

    {{-- FAQ  --}}
    @include('partials.faq')

    {{-- FOOTER  --}}
    @include('partials.footer')
@endsection
