@extends('layouts.main')

@section('title', 'BTS-ITSK | Detail Kegiatan')

@section('content')
    {{-- NAVBAR  --}}
    @include('partials.navbar')

    {{-- DETAI KEGIATAN  --}}
    @include('partials.detailkegiatan')

    {{-- FOOTER  --}}
    @include('partials.footer')

@endSection
