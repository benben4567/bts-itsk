@extends('admin.dashboard.layouts.main')

@php
    $title = 'Kegiatan';
@endphp

@section('title')
    Dashboard Edit Kegiatan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 col-sm-10 col-md-9 mx-auto mb-5 p-4 p-sm-5 border"
        style="background-color: rgb(255, 255, 255); margin-top: 125px; border-radius: 10px">
        <form action="#" method="post">
            <h1 class="fs-5 mb-5 pb-2 border-bottom border-2">Edit Data Kegiatan</h1>

            <div class="mb-4">
                <label for="ketua" class="fw-semibold mb-2">Nama Ketua</label>
                <select id="ketua" class="form-select p-2" name="ketua" required>
                    <option value="">Ketua 1</option>
                    <option value="" selected>Ketua 2</option>
                    <option value="">Ketua 3</option>
                </select>
            </div>

            {{-- <div class="mb-4">
                <label for="ketua" class="form-label fw-semibold">Nama Ketua</label>
                <input type="text" class="form-control p-2" id="ketua" name="ketua"
                    placeholder="Masukkan Nama Ketua" value="Budi Santoso" required>
            </div> --}}

            <div class="mb-4">
                <label for="dosen" class="fw-semibold mb-2">Nama Dosen</label>
                <select id="dosen" class="form-select p-2" name="dosen" required>
                    <option value="">Dosen 1</option>
                    <option value="" selected>Dosen 2</option>
                    <option value="">Dosen 3</option>
                </select>
            </div>

            {{-- <div class="mb-4">
                <label for="dosen" class="form-label fw-semibold">Nama Dosen</label>
                <input type="text" class="form-control p-2" id="dosen" name="dosen"
                    placeholder="Masukkan Nama Dosen" value="Ahmad Wibowo, S.Kom., M.Kom" required>
            </div> --}}

            <div class="mb-4">
                <label for="sekolah" class="fw-semibold mb-2">Nama Sekolah</label>
                <select id="sekolah" class="form-select p-2" name="sekolah" required>
                    <option value="">Sekolah 1</option>
                    <option value="" selected>Sekolah 2</option>
                    <option value="">Sekolah 3</option>
                </select>
            </div>

            {{-- <div class="mb-4">
                <label for="sekolah" class="form-label fw-semibold">Nama Sekolah</label>
                <input type="text" class="form-control p-2" id="sekolah" name="sekolah"
                    placeholder="Masukkan Nama Sekolah" value="SMA Negeri 1 Jakarta" required>
            </div> --}}

            <div class="mb-4">
                <label for="tanggal_kegiatan" class="form-label fw-semibold">Tanggal Kegiatan</label>
                <input type="date" class="form-control p-2" id="tanggal_kegiatan" name="tanggal_kegiatan"
                    placeholder="Masukkan Tanggal Kegiatan" value="2024-02-16" required>
            </div>

            <div class="mb-0">
                <label for="anggota" class="fw-semibold mb-2">Nama Anggota</label>
                <select id="anggota" class="form-select p-2" name="anggota" required>
                    <option value="">Anggota 1</option>
                    <option value="" selected>Anggota 2</option>
                    <option value="">Anggota 3</option>
                </select>
            </div>

            <button type="submit" class="btn btn-dark d-block mx-auto mt-5 px-5 py-2">Simpan</button>
        </form>
    </div>
@endsection