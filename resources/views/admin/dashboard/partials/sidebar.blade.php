<li class="nav-item">
    <a href="{{ route('admin.dashboard') }}"
        class="nav-link text-light {{ $title === 'Admin' ? 'active' : '' }}">
        <span style="margin-right: 3px"><i class="bi bi-house-door"></i></span>
        Beranda
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('usersmanagement.index') }}"
        class="nav-link text-light {{ $title === 'User' ? 'active' : '' }}">
        <span style="margin-right: 3px"><i class="bi bi-person"></i></span>
        User
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('Admin.file_pendukung.view') }}"
        class="nav-link text-light {{ $title === 'File Pendukung' ? 'active' : '' }}">
        <span style="margin-right: 3px"><i class="bi bi-folder"></i></span>
        File Pendukung
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.kegiatan.index') }}"
        class="nav-link text-light {{ $title === 'Kegiatan' ? 'active' : '' }}">
        <span style="margin-right: 3px"><i class="bi bi-calendar-event"></i></span>
        Kegiatan
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.dashboard.laporan.laporan_index') }}"
        class="nav-link text-light {{ $title === 'Laporan' ? 'active' : '' }}">
        <span style="margin-right: 3px;"><i class="bi bi-journal-text"></i></span>
        Laporan
    </a>
</li>