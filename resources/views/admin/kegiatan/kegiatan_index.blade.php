@extends('admin.dashboard.layouts.main')

@php
    $title = 'Kegiatan';
@endphp

@section('title')
    Dashboard Kegiatan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 mx-auto mb-5 border overflow-hidden"
        style="background-color: rgb(255, 255, 255); font-size: 13px; margin-top: 125px; border-radius: 10px">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center p-4">
            <h1 class="fs-5 mb-3 mb-sm-0">Data Kegiatan</h1>
        </div>
        {{-- <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center text-secondary px-4">
            <form action="{{ route('admin.kegiatan.live-search') }}" method="get" class="">
                @csrf
                <label for="search">Search :</label>
                <input type="text" name="search" id="search" value="{{ request('search') }}"
                    class="border border-2 px-1" style="width: 200px; border-radius: 5px;">
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div> --}}
        @if (session('success'))
            <div class="mt-3 alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        {{-- Table --}}
        {{-- @if ($kegiatan->isEmpty())
            <p class="m-4">Tidak ada kegiatan.</p>
        @else --}}
        <div class="table-responsive px-3 pb-3">
            <table class="table table-hover mt-3" id="table-kegiatan">
                <thead class="table-light border-top border-bottom">
                    <tr>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">NO</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">NAMA KETUA</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">NAMA DOSEN</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">PROVINSI</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">TANGGAL KEGIATAN</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">SEKOLAH</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">STATUS</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">AKSI</th>
                    </tr>
                </thead>

                <tbody id="kegiatanTableBody">
                    @forelse ($kegiatan as $item)
                        <tr>
                            <td class="text-secondary text-center px-3 text-nowrap">{{ $loop->iteration }}</td>
                            <td class="px-3 text-nowrap">
                                @foreach ($item->users()->where('jabatan', 'Ketua')->get() as $anggota)
                                    {{ $anggota->nama }}
                                @endforeach
                            </td>
                            <td class="px-3 text-nowrap">
                                @foreach ($item->users()->where('jabatan', 'Dosen')->get() as $anggota)
                                    {{ $anggota->nama }}
                                @endforeach
                            </td>
                            <td class="px-3 text-nowrap">{{ $item->provinsi->provinsi }}</td>
                            <td class="px-3 text-nowrap">{{ $item->tanggal_kegiatan }}</td>
                            <td class="px-3 text-nowrap">{{ $item->sekolah }}</td>
                            <td class="px-3 text-center text-nowrap">
                                @if ($item->status_promosi == 'Proses')
                                    <span class="badge text-bg-warning fw-normal pb-2"
                                        style="font-size: 13px">Diproses</span>
                                @elseif($item->status_promosi == 'Diterima')
                                    <span class="badge text-bg-success fw-normal pb-2"
                                        style="font-size: 13px">Diterima</span>
                                @elseif($item->status_promosi == 'Ditolak')
                                    <span class="badge text-bg-danger fw-normal pb-2" style="font-size: 13px">Ditolak</span>
                                @else
                                    <span class="badge text-bg-secondary fw-normal pb-2"
                                        style="font-size: 13px">{{ $item->status_promosi }}</span>
                                @endif
                            </td>
                            <td class="text-center px-3 text-nowrap">
                                <form action="{{ route('kegiatan.show', ['id' => $item->id]) }}" method="get"
                                    class="d-inline">
                                    <button type="submit" name="lihat" class="btn text-secondary fs-5 mx-1">
                                        <i class="bi bi-eye"></i>
                                    </button>
                                </form>
                                <form action="{{ route('admin.kegiatan.edit', $item->id) }}" method="get"
                                    class="d-inline">
                                    <button type="submit" name="edit" class="btn text-secondary fs-5 mx-1">
                                        <i class="bi bi-pencil-square"></i>
                                    </button>
                                </form>
                                <form action="{{ route('admin.kegiatan.destroy', $item->id) }}" method="post"
                                    class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" name="hapus" class="btn text-secondary fs-5 mx-1"
                                        onclick="return confirm('Apakah Anda yakin ingin menghapus kegiatan ini?')">
                                        <i class="bi bi-trash3"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- @endif --}}

        {{-- <div class="d-flex flex-column flex-md-row justify-content-between align-items-center text-secondary p-4">
            <div>
                Showing {{ $kegiatan->firstItem() }} to {{ $kegiatan->lastItem() }} of {{ $kegiatan->total() }} entries
            </div>
            <nav class="mt-5 mt-md-0">
                <ul class="pagination mb-0">
                    @if ($kegiatan->onFirstPage())
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link text-secondary"
                                href="{{ $kegiatan->previousPageUrl() }}&search={{ request('search') }}">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @endif

                    @for ($i = 1; $i <= $kegiatan->lastPage(); $i++)
                        <li class="page-item {{ $i == $kegiatan->currentPage() ? 'active' : '' }}">
                            <a class="page-link"
                                href="{{ $kegiatan->url($i) }}&search={{ request('search') }}">{{ $i }}</a>
                        </li>
                    @endfor

                    @if ($kegiatan->hasMorePages())
                        <li class="page-item">
                            <a class="page-link text-secondary"
                                href="{{ $kegiatan->nextPageUrl() }}&search={{ request('search') }}">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @else
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div> --}}
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table-kegiatan').DataTable();
        });
    </script>
@endpush