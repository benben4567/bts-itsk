@extends('admin.dashboard.layouts.main')

@php
    $title = 'Kegiatan';
@endphp

@section('title')
    Dashboard Kegiatan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    @if($kegiatans->isEmpty())
        <p>Tidak ada kegiatan ditemukan untuk pencarian: "{{ $search }}"</p>
    @else
    <div class="table-responsive" style="margin-top: 100px; overflow-x: auto;">
        <h4>Hasil Pencarian</h4>
        <table class="table table-hover" style="min-width: 600px;">
            <thead class="table-light border-top border-bottom">
                <tr>
                    <th class="text-secondary text-center px-3 text-nowrap">No</th>
                    <th class="px-3 text-nowrap">Provinsi</th>
                    <th class="px-3 text-nowrap">Tanggal Kegiatan</th>
                    <th class="px-3 text-nowrap">Sekolah</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kegiatans as $kegiatan)
                    <tr>
                        <td class="text-secondary text-center px-3 text-nowrap">{{ $loop->iteration }}</td>
                        <td class="px-3 text-nowrap">{{ $kegiatan->provinsi->provinsi }}</td>
                        <td class="px-3 text-nowrap">{{ $kegiatan->tanggal_kegiatan }}</td>
                        <td class="px-3 text-nowrap">{{ $kegiatan->sekolah }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>    
    @endif
@endsection