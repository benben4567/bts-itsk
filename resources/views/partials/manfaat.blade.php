<section id="manfaat">
    <div class="container pb-5 pt-1">
        <h1 class="pb-3 pt-3">Manfaat</h1>
        <div class="row">
            <div class="col-md-4">
                <div class="card text-center p-3 mx-3">
                    <div class="card-body">
                        <img src="img/benefit.png" alt="">
                        <h5 class="card-title pt-3">Mendapat Benefit Dari Kampus</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center p-3 mx-3">
                    <div class="card-body">
                        <img src="img/pengalaman.png" alt="">
                        <h5 class="card-title pt-3">Mendapatkan Pengalaman Baru</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center p-3 mx-3">
                    <div class="card-body">
                        <img src="img/relasi.png" alt="">
                        <h5 class="card-title pt-3">Memperluas Relasi</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
