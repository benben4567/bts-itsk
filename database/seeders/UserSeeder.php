<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'nama' => 'budi',
                'nim' => '123456789012',
                'email' => 'budz@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRPL',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'rashel',
                'nim' => '123456789013',
                'email' => 'rash@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRPL',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'riznal',
                'nim' => '123456789014',
                'email' => 'rizz@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRPL',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Jonathan Joestar',
                'nim' => '123456789015',
                'email' => 'jonathan@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRPL',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Joseph Joestar',
                'nim' => '123456789016',
                'email' => 'joseph@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRPL',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Dio Brando',
                'nim' => '123456789022',
                'email' => 'dio@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRK',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Kars',
                'nim' => '123456789023',
                'email' => 'kars@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRK',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Enrico Pucci',
                'nim' => '123456789024',
                'email' => 'pucci@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRK',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Yoshikage Kira',
                'nim' => '123456789025',
                'email' => 'kira@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRK',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Diavolo',
                'nim' => '123456789026',
                'email' => 'diavolo@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'TRK',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Funny Valentine',
                'nim' => '123456789032',
                'email' => 'valentine@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'BD',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Diego Brando',
                'nim' => '123456789033',
                'email' => 'diego@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'BD',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Gyro Zeppeli',
                'nim' => '123456789034',
                'email' => 'gyro@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'BD',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Johnny Joestar',
                'nim' => '123456789035',
                'email' => 'johnny@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'BD',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Higashikata Josuke',
                'nim' => '123456789036',
                'email' => 'josuke@example.com',
                'password' => Hash::make('password'),
                'role' => 'mahasiswa',
                'prodi' => 'BD',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Weather Report',
                'nim' => '987654321097',
                'email' => 'weather@example.com',
                'password' => Hash::make('password123'),
                'role' => 'dosen',
                'prodi' => 'TRPL',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Jane Doe',
                'nim' => '987654321098',
                'email' => 'jane@example.com',
                'password' => Hash::make('password123'),
                'role' => 'dosen',
                'prodi' => 'TRK',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Jolyne Joestar',
                'nim' => '987654321099',
                'email' => 'jolyne@example.com',
                'password' => Hash::make('password123'),
                'role' => 'dosen',
                'prodi' => 'BD',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'nama' => 'Admin Sudah Datang',
                'nim' => '123123123123',
                'email' => 'admin1@example.com',
                'password' => Hash::make('password123'),
                'role' => 'admin',
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            // Tambahkan entri lain jika perlu
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
